"use strict";

var app = (function(){
    var el = document.getElementById('countries');
    var countries = ['France', 'Germany', 'England'];

    var fetchAll = function(){
        var data = '';
        
        if (countries.length > 0) {
          for (var i = 0; i < countries.length; i++) {
            data += '<tr>';
            data += '<td>' + countries[i] + '</td>';
            data += '<td><button onclick="app.Edit(' + i + ')">Edit</button></td>';
            data += '<td><button onclick="app.Delete(' + i + ')">Delete</button></td>';
            data += '</tr>';
          }
        }
        
        return el.innerHTML = data;
    };

    var add = function(){
        var countryName = document.getElementById('add-name');
        var country = countryName.value;
        if (country) {
        countries.push(country.trim());        
        countryName.value = '';        
        fetchAll();
        }
    };

    var edit = function (item) {
              var editName = document.getElementById('edit-name');
              editName.value = countries[item];
              document.getElementById('spoiler').style.display = 'block';
              self = this;
              document.getElementById('saveEdit').onsubmit = function() {                
                var country = editName.value;
                if (country) {
                  countries.splice(item, 1, country.trim());
                  fetchAll();
                  // Hide fields
                  closeInput();
                }
              }
            };

    var closeInput = function(){
        document.getElementById('spoiler').style.display = 'none';
    };
    var deleteItem = function(item){
        countries.splice(item, 1);
        fetchAll();
    };

    return {        
        FetchAll: fetchAll,
        Add: add,
        Edit:edit,
        CloseInput:closeInput,
        Delete:deleteItem

    }

})();